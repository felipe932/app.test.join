import Vue from 'vue'

export const token = '_joinAdminToken'
export const userKey = '_joinAdminLogged'
export const baseApiUrl =  process.env.VUE_APP_APIURL;

export function showError(e) {
    if(e && e.response && e.response.data) {
        if(e.response.data.message) 
            Vue.toasted.global.defaultError({ msg : e.response.data.message })
        else
            Vue.toasted.global.defaultError()
    } else if(typeof e === 'string') {
        Vue.toasted.global.defaultError({ msg : e })
    } else {
        Vue.toasted.global.defaultError()
    }
}

export default { baseApiUrl, showError, userKey, token }