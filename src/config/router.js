import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/components/home/Home'
import Users from '@/components/users/Users'
import UsersForm from '@/components/users/UsersForm'

import CategoriesPages from '@/components/categories/CategoriesPages'
import Products from '@/components/products/Products'
import ProductsForm from '@/components/products/ProductsForm'

import Auth from '@/components/auth/Auth'

Vue.use(VueRouter)

const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'users',
        path: '/users',
        component: Users
    },
    {
        name: 'users-create',
        path: '/users/form',
        component: UsersForm
    },
    { 
        name: 'users-update',
        path: '/users/form/:id',
        component: UsersForm
    },
    {
        name: 'auth',
        path: '/auth',
        component: Auth
    },
    {
        name: 'categories',
        path: '/categories',
        component: CategoriesPages
    },
    {
        name: 'products',
        path: '/products',
        component: Products
    },
    {
        name: 'products-create',
        path: '/products/form',
        component: ProductsForm
    },
    {
        name: 'products-update',
        path: '/products/form/:id',
        component: ProductsForm
    },
];

export default new VueRouter({
    mode: 'history',
    routes
});