# frontend

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Send to firebase (Remender to update the hoster)
```
firebase deploy --only hosting:saoluis-admin
```